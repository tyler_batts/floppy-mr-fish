# README #

### What is this repository for? ###

This game was built for Indie Speed Run 2015, the 48 hour game jam. The theme given was Elevators. So we decided upon a game where you take on the role of a goldfish who is trapped in an office building and is trying to reach the elevator to escape. After the jam was finished, I took the game, fixed a few bugs, and optimized it to play well on the OUYA. The OUYA, in terms of hardware specs, isn't very high-performance. Therefore the game required a lot of tweaking of logic and a lot of tweaking of the environment's setup. 

For this project, the team was comprised of myself as the lead developer, Zach George the 3D artist/animator, and Anthony DelSordo the texture artist and second developer. 

### How do I get set up? ###

This version of the game was built for the OUYA, but can be played on a PC with a keyboard

Q: Flop left

W: Jump

E: Flop right

Left/Right Arrows: rotate the camera left and right

Up/Down Arrows: zoom the camera in and out

The game executable can be found  in the downloads section of this repository!
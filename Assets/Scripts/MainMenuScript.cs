﻿using UnityEngine;
using System.Collections;

public class MainMenuScript : MonoBehaviour {

	public void LoadGame() {
		Application.LoadLevel("Intro");
	}

	public void LoadCredits() {
		Application.LoadLevel("Credits");
	}

	public void QuitGame() {
		Application.Quit();
	}

	void Update() {
		if (Input.GetButtonDown("Submit")) {
			Application.LoadLevel("Intro");
		}
	}
}

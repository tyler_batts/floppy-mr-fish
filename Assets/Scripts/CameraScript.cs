﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	public GameObject player, ceiling;
	public float rotationSpeed, dollySpeed;
	public float maxDistance;
	public bool gameOver;

	float diff;

	void Start () {
		Screen.SetResolution(640, 480, true);
		if (player == null) {
			player = GameObject.FindGameObjectWithTag("Player");
		}

		if (!ceiling.activeInHierarchy)
			ceiling.SetActive(true);

		Vector3 pos = transform.position;
		pos = player.transform.position + (new Vector3(-10, 2, 0));
		transform.position = pos;
	}

	void FixedUpdate() {
		diff = (player.transform.position - transform.position).magnitude;

		transform.LookAt(player.transform.position);
		transform.RotateAround(player.transform.position, Vector3.up, -1*Input.GetAxis("Horizontal"));

		if (diff < 2f) {
			if (Input.GetAxis ("Vertical") < 0) {
				transform.position = Vector3.MoveTowards (transform.position, player.transform.position, dollySpeed * Input.GetAxis ("Vertical"));
			}
		} else if (diff >= 2f && diff <= maxDistance) {
			if (Mathf.Abs (Input.GetAxis ("Vertical")) > 0) {
				transform.position = Vector3.MoveTowards (transform.position, player.transform.position, dollySpeed * Input.GetAxis ("Vertical"));
			}
		} else {
			if (Input.GetAxis ("Vertical") > 0) {
				transform.position = Vector3.MoveTowards (transform.position, player.transform.position, dollySpeed * Input.GetAxis ("Vertical"));
			}
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class IntroScript : MonoBehaviour {
	public GameObject instructions;

	void Update() {
		if (Input.GetButtonDown("Submit") || Input.GetMouseButtonDown(0)) {
			if (!instructions.activeInHierarchy)
				instructions.SetActive(true);
			else
				Application.LoadLevel("Level1");
		}

		if (Input.GetButtonDown("Cancel")) {
			Application.LoadLevel("Level1");
		}
	}
}

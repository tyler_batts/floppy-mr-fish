﻿using UnityEngine;
using System.Collections;

public class CollisionObject: MonoBehaviour {

	public AudioSource collisionSound;

	void OnCollisionEnter(Collision other) {
		if (other.collider.GetComponent<Rigidbody>() != null) {
			if (!collisionSound.isPlaying && other.collider.GetComponent<Rigidbody>().velocity.magnitude > 0)
				collisionSound.Play();
		}
	}
}

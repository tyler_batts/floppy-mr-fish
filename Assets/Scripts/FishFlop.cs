using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FishFlop : MonoBehaviour {

	public GameObject spawnLocation;
	public float flapForce, sideToSideScalar;
	public Camera cam;
	public Text respawnTimer, timerText;
	public float maxTimeInSeconds;
	public GameObject gameOver;

	[Range(0, 1)]
	public float assistPush;

	[SerializeField]
	private Rigidbody rbFin, rbMid, rbHead;
	private bool grounded;

	public float restartTimer;
	private float timer;
	private bool isTiming;

	[SerializeField]
	private float timer2;
	private bool isTiming2;

	void Start() {
		if (cam == null)
			cam = Camera.main;

		if (spawnLocation == null)
			spawnLocation = GameObject.FindGameObjectWithTag("Spawn");

		StartTimer2();
	}

	void Update() {
		Flap();
		RunTimer2();


		#region Play Timer
		if ((maxTimeInSeconds-timer2)%60 < 10) {
			timerText.text = ((int)(maxTimeInSeconds-timer2)/60 + ":0" + (int)(maxTimeInSeconds-timer2)%60);
		} else {
			timerText.text = ((int)(maxTimeInSeconds-timer2)/60 + ":" + (int)(maxTimeInSeconds-timer2)%60);
		}

		if (timer2 >= maxTimeInSeconds) {
			if (!gameOver.activeInHierarchy) {
				gameOver.SetActive(true);
				rbFin.isKinematic = true;
				rbMid.isKinematic = true;
				rbHead.isKinematic = true;

				if (Input.GetButtonDown("Submit")) {
					transform.position = spawnLocation.transform.position;
					transform.rotation = Quaternion.Euler(new Vector3(0,90,0));
					cam.transform.position = transform.position + new Vector3(-10, 2, 0);
					StartTimer2();

					gameOver.SetActive(false);
					rbFin.isKinematic = false;
					rbMid.isKinematic = false;
					rbHead.isKinematic = false;
				} else if (Input.GetButtonDown("Cancel")) {
					Application.LoadLevel("Menu");
				}
				StopTimer2();
			}
		}
		#endregion

		#region Restart Button
		if (Input.GetButtonDown("Restart")) {
			StartTimer();
		}
		if (Input.GetButton("Restart")) {
			RunTimer();

			if (isTiming) {
				respawnTimer.gameObject.SetActive(true);
				respawnTimer.text = ((int)restartTimer - (int)timer).ToString();
			}
		}
		if (Input.GetButtonUp("Restart")) {
			StopTimer();
			respawnTimer.gameObject.SetActive(false);
		}

		if (timer > restartTimer) {
			transform.position = spawnLocation.transform.position;
			transform.rotation = Quaternion.Euler(new Vector3(0,90,0));
			cam.transform.position = transform.position + new Vector3(-10, 2, 0);

			StopTimer();
			timer = 0;
			respawnTimer.gameObject.SetActive(false);
			StartTimer2();
		}
		#endregion
	}

	#region Timer
	void StartTimer() {
		timer = 0;
		isTiming = true;
	}

	void RunTimer() {
		if (isTiming)
			timer += Time.deltaTime;
	}

	void StopTimer() {
		isTiming = false;
	}
	#endregion

	#region Timer2
	void StartTimer2() {
		timer = 0;
		isTiming2 = true;
	}
	
	void RunTimer2() {
		if (isTiming2)
			timer2 += Time.deltaTime;
	}
	
	void StopTimer2() {
		isTiming2 = false;
	}
	#endregion
		
	#region Movement
	void Flap() {
		if (Input.GetButtonDown("Middle") && (rbMid.GetComponent<SegmentScript>().JumpCount == 0)) {
			rbMid.AddForce((-Vector3.down * flapForce), ForceMode.VelocityChange);
			rbMid.GetComponent<SegmentScript>().JumpCount++;
		}

		if (Input.GetButtonDown("Tail") && (rbFin.GetComponent<SegmentScript>().JumpCount < rbFin.GetComponent<SegmentScript>().jumpMax) && !rbFin.GetComponent<SegmentScript>().Grounded) {
			rbFin.AddForce((cam.transform.rotation*Vector3.left.normalized * flapForce * sideToSideScalar), ForceMode.VelocityChange);
			rbFin.GetComponent<SegmentScript>().JumpCount++;
		}
		
		if (Input.GetButtonDown("Head") && rbHead.GetComponent<SegmentScript>().JumpCount < rbFin.GetComponent<SegmentScript>().jumpMax && !rbHead.GetComponent<SegmentScript>().Grounded) {
			rbHead.AddForce((cam.transform.rotation*Vector3.right.normalized * flapForce * sideToSideScalar), ForceMode.VelocityChange);
			rbHead.GetComponent<SegmentScript>().JumpCount++;
		}
	}
	#endregion

}
﻿using UnityEngine;
using System.Collections;

public class SegmentScript : MonoBehaviour {

	public int jumpMax;
	public float raycastDistance;
	private bool grounded;
	private Rigidbody rb;

	[SerializeField]
	private AudioSource[] flop = new AudioSource[5];
	private int jumpCount = 0;

	void Start() {
		rb = GetComponent<Rigidbody>();
	}

	void Update() {
		RaycastHit hit;
		Physics.Raycast(transform.position, -Vector3.up, out hit, raycastDistance);
		Debug.DrawRay(transform.position, -Vector3.up, Color.green, 0.01f);

		if (hit.collider != null) {
			if (hit.collider.tag == "Environment") {
				jumpCount = 0;
				grounded = true;
			}
		}
		else
			grounded = false;
	}

	void OnGizmosDraw() {

	}

	void OnCollisionEnter(Collision other) {
		int rando = Random.Range(0, 4);
		if (!flop[rando].isPlaying) {
			flop[rando].Play();
		}
	}

	public bool Grounded {
		get {
			return grounded;
		}
	}

	public int JumpCount {
		get {
			return jumpCount;
		}

		set {
			jumpCount = value;
		}
	}
}

﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class Elevator : MonoBehaviour {

	public ElevatorScript controller;
	public float elevatorWaitTime;
	public GameObject levelCompleteUI;

	private bool isTiming, dingClosed;
	private float timer;

	void Update() {
		RunTimer();
	}

	#region Timer
	void StartTimer() {
		timer = 0;
		isTiming = true;
	}

	void RunTimer() {
		if (isTiming)
			timer += Time.deltaTime;
	}

	void StopTimer() {
		isTiming = false;
	}
	#endregion

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			StartTimer();
		}
	}

	void OnTriggerStay(Collider other) {
		if (other.tag == "Player" && (timer > elevatorWaitTime)) {
			controller.DoorsOpening = false;
			if (controller.DoorsClosed) {
				Debug.Log("Level Complete!");
				levelCompleteUI.SetActive(true);

				if (Input.GetButtonDown("Submit")) {
					SceneManager.LoadScene("Menu");
				}
			}
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class DJRoomba : MonoBehaviour {

	public float moveSpeed; 
	public AudioSource song;
	public GameObject GameOver;
	Rigidbody rb;
	Vector3 direction;
	
	void Start () {
		rb = GetComponent<Rigidbody>();
		direction = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f)).normalized;
		rb.AddForce(direction*moveSpeed, ForceMode.VelocityChange);
	}

	void FixedUpdate () {
		if (rb.velocity.magnitude < moveSpeed) {
			rb.velocity = (rb.velocity.normalized * moveSpeed);
		}
	}
}

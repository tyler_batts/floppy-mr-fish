﻿using UnityEngine;
using System.Collections;

public class Monitor : MonoBehaviour {

	public AudioSource collisionSound;
	
	void OnCollisionEnter(Collision other) {
		if (!collisionSound.isPlaying)
			collisionSound.Play();
	}
}

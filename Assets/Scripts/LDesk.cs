﻿using UnityEngine;
using System.Collections;

public class LDesk : MonoBehaviour {

	public AudioSource typing;

	void Start() {
		Invoke("Typing", Random.Range(0f, 5f));
	}

	void Typing() {
		typing.Play();
	}
}

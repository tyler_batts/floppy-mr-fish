﻿using UnityEngine;
using System.Collections;

public class ElevatorScript : MonoBehaviour {

	public GameObject leftDoor, rightDoor;
	public float openSpeed, doorLimit;
	public AudioSource doorDing;
	
	private bool doorsOpening;
	private bool doorsClosed;

	private bool dingOpen, dingClosed;

	Vector3 leftDoorStart, leftDoorStop, rightDoorStart, rightDoorStop;

	void Start() {
		leftDoorStart = leftDoor.transform.position;
		rightDoorStart = rightDoor.transform.position;

		leftDoorStop = leftDoorStart + Vector3.left*doorLimit;
		rightDoorStop = rightDoorStart + Vector3.right*doorLimit;
	}
	void FixedUpdate() {
		//Debug.Log(doorsOpening);
		if (doorsOpening) {
			if (!dingOpen && !doorDing.isPlaying) {
				doorDing.Play ();
				dingOpen = true;
			}
			leftDoor.transform.position = Vector3.MoveTowards(leftDoor.transform.position, leftDoorStop, openSpeed);
			rightDoor.transform.position = Vector3.MoveTowards(rightDoor.transform.position, rightDoorStop, openSpeed);
		} else if (!doorsOpening) {
			if (!dingClosed && dingOpen && !doorDing.isPlaying) {
				doorDing.Play ();
				dingClosed = true;
			}
			leftDoor.transform.position = Vector3.MoveTowards(leftDoor.transform.position, leftDoorStart, openSpeed);
			rightDoor.transform.position = Vector3.MoveTowards(rightDoor.transform.position, rightDoorStart, openSpeed);
		}

		if (leftDoor.transform.position == leftDoorStart && rightDoor.transform.position == rightDoorStart) {
			doorsClosed = true;
		} else {
			doorsClosed = false;
		}
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			doorsOpening = true;
		}
	}

	void OnTriggerExit(Collider other) {
		if (other.tag == "Player") {
			doorsOpening = false;
		}
	}

	public bool DoorsClosed {
		get {
			return doorsClosed;
		}
	}

	public bool DoorsOpening {
		get {
			return doorsOpening;
		}

		set {
			doorsOpening = value;
		}
	}
}
